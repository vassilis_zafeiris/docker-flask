# Run Jupyter from docker

Jupyter can be executed as docker container. 

You can try it yourself by following the steps below:

1. Create a directory with name `jupyter` inside the `/home/user/` directory of the VM.
2. Pull a jupyter image from dockerHub, e.g. jupyter/scipy-notebook:7d427e7a4dde
3. Create a container from the jupyter image with the following specifications 
    - it exports port 8888 which is mapped to its internal port 8888
    - it maps the `/home/jovyan/output` directory of the container to the `notebooks` subdirectory of `jupyter` directory
4. Run the container

Try to create and run the container with plain docker commands.
Then create a `docker-compose.yml` file to spawn jupyter with a plain `docker-compose up` command.

Direct the browser to the URL http://localhost:8888
    
    

